README
======

This is a blank project to allow creating a new project a lot quicker and possibly automated


Setting Up Your VHOST
=====================

The following is a sample VHOST you might want to consider for your project.

<VirtualHost *:80>
   DocumentRoot "${FOLDER_PATH}"
   ServerName ${SERVER_NAME}

   # This should be omitted in the production environment
   SetEnv APPLICATION_ENV local

   <Directory "${FOLDER_PATH}/public">
       Options Indexes MultiViews FollowSymLinks
       AllowOverride All
       Order allow,deny
       Allow from all
   </Directory>

</VirtualHost>
