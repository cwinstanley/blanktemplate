<?php

class Plugin_Layout extends Zend_Controller_Plugin_Abstract
{
   public function preDispatch(Zend_Controller_Request_Abstract $request)
   {
	  /* Get a reference to the layout and the view */
      $layout = Zend_Layout::getMvcInstance();
      $view = $layout->getView();

   }

}

