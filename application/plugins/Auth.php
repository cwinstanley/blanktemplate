<?php

class Plugin_Auth {

	public static function isLoggedIn()
    {
        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity();
            return true;
        }
        return false;
    }

}

