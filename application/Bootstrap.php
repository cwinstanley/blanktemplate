<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    /**
     * Setup the autoloaders so that they will load from some extra directories for;
     *  Forms
     *  Models
     *  Plugins
     *
     * @return Zend_Application_Module_Autoloader
     */
    protected function _initAutoloader()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => '',
            'basePath'  => APPLICATION_PATH,
            'resourceTypes' => array (
                'form' => array(
                    'path' => 'forms',
                    'namespace' => 'Form',
                ),
                'model' => array(
                    'path' => 'models',
                    'namespace' => 'Model',
                ),
				'plugin' => array(
					'path' => 'plugins',
					'namespace' => 'Plugin'
				),
            )
        ));

        /* Add the Util_ directory to the auto loader */
		$al = Zend_Loader_Autoloader::getInstance();
		$al->registerNamespace('Util_');

        return $autoloader;
    }

    /**
     * Setup the view helpers, this will allow us to use the layout meta data functions etc
     * Set the title of the html doc and anything generally related to the layout/view
     *
     */
    protected function _initViewHelpers()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        $view->doctype('XHTML1_STRICT');
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
        $view->headMeta()->appendHttpEquiv('keywords', 'blank,template,zend');
        $view->headMeta()->appendHttpEquiv('description', 'Blank template for zend framework 1.12');
        $view->headTitle()->setSeparator(' - ');
        $view->headTitle('Zend BlankTemplate');

		// Add the image view helper library
		$view->addHelperPath('Util/View/Helper', 'Util_View_Helper');

		// Register front controller plugin so we can access some variables in the layout
		Zend_Controller_Front::getInstance()->registerPlugin(new Plugin_Layout());
    }

    /**
     * TODO: Load navigation from navigation.ini file
     */
    protected function _initNavigation()
    {
		
    }

}

