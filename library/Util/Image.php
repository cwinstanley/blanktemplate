<?php

class Util_Image {
	public function thumbnail($filename, $width, $height)
	{
		// first hash the filename so we can check the cache
		$hash     = md5($filename) . "{$width}x{$height}";
		$file_ext = pathinfo($filename, PATHINFO_EXTENSION);
		// cache path
		$cache = realpath(APPLICATION_PATH . '/../public/uploads/cache');

		// check if the image exists
		if (file_exists($filename)) {
			// check if the cached image already exists
			if (file_exists("{$cache}/{$hash}.{$file_ext}")) {
				// file exists so echo out location to cached file
				return "<img src='/uploads/cache/{$hash}.{$file_ext}' />";
			}
			else {
				if (class_exists('Imagick')) {
					$image = new Imagick($filename);
					$image->thumbnailImage($width, $height, true);
					$image->writeImage("{$cache}/{$hash}.{$file_ext}");
				}
				else {
					$new_filename = "{$cache}/{$hash}.{$file_ext}";
					system("/opt/local/bin/convert " . escapeshellarg($filename) . " -resize {$width}x{$height} " . escapeshellarg($new_filename) );
				}
				return "<img src='/uploads/cache/{$hash}.{$file_ext}' />";
			}
		}
		else {
			// file doesnt exist so cant thumbnail it so error log
			error_log('Unable to create thumbnail, file does not exist ' . $filename);
		}
	}
}
