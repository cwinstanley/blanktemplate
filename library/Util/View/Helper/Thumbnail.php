<?php
/**
 * @require Util_Image
 */
class Util_View_Helper_Thumbnail extends Zend_View_Helper_Abstract
{
	public function thumbnail($filename, $width, $height)
	{
		return Util_Image::thumbnail($filename, $width, $height);
	}
}